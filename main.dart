import 'package:codelab_flutter_fb_ui_clone/config/my_theme.dart';
import 'package:codelab_flutter_fb_ui_clone/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Facebook Clone',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: const Color(0xFFCCCCCC),
          appBarTheme: const AppBarTheme(
            backgroundColor: Colors.white,
            systemOverlayStyle: SystemUiOverlayStyle(
              statusBarColor: Colors.transparent,
              statusBarIconBrightness: Brightness.dark,
            ),
          ),
          textTheme: const TextTheme(
            headline1: TextStyle(
              fontSize: 26,
              color: MyTheme.facebookBlue,
              fontWeight: FontWeight.bold,
              letterSpacing: -1.5,
            ),
            headline2: TextStyle(
                fontSize: 16,
                color: MyTheme.primaryTextColor,
                fontWeight: FontWeight.bold),
            bodyText1: TextStyle(
                fontSize: 16,
                color: MyTheme.primaryTextColor,
                fontWeight: FontWeight.normal),
            bodyText2: TextStyle(
              fontSize: 16,
              color: MyTheme.primaryTextColor,
              fontWeight: FontWeight.normal,
            ),
            subtitle1: TextStyle(
                fontSize: 12,
                color: MyTheme.primaryTextColor,
                fontWeight: FontWeight.w300),
            subtitle2: TextStyle(
                fontSize: 12,
                color: MyTheme.primaryTextColor,
                fontWeight: FontWeight.w300),
          )),
      home: const HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
