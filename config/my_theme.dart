import 'package:flutter/material.dart';

class MyTheme {
  static const Color scaffold = Color(0xFFF0F2F5);

  static const Color facebookBlue = Color(0xFF1777F2);

  static const Color primaryTextColor = Color(0xFF454545);

  static const Color grey = Color(0xFF9D9C9C);

  static const LinearGradient storyGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Colors.transparent, Colors.black26],
  );
}
