# codelab_flutter_fb_ui_clone

 resources for "build Facbook UI clone with Flutter" Codelab

## Getting Started

## Set up your Flutter environment

You need two pieces of software to complete this lab—the [Flutter SDK](https://docs.flutter.dev/get-started/install) and [an editor](https://docs.flutter.dev/get-started/editor). (The codelab assumes that you're using VSCode, but you can use your preferred editor.)

If you are fail to setup the Flutter env in your computer, you could use the online sandbox environment tool (https://zapp.run) to participate in the codelab. 

### Create the starter Flutter app
Create a simple, templated Flutter app. Create a Flutter project called startup_namer as follows.

```shell
$ flutter create codelab_flutter_fb_ui_clone
$ cd codelab_flutter_fb_ui_clone
```
